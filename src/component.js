import React from 'react';

class MyDiv extends React.Component {
  
  redirect() {
    window.location.href = 'https://www.youtube.com/watch?v=K9V-K3uYfis';
  }
  
  render() {
    return (
      <div
        className="tzafrir"
        onClick={this.redirect}
        style={{backgroundColor:'red'}}>
        BULLSHIT
      </div>
    );
  }
}

export default MyDiv;