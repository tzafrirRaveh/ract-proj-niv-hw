/*
 This file is the entry point for the webpack dev server,
 Your code is safe! - You can change this file to contain whatever you want.
 To get started, run 'npm start'.

 IMPORTANT !!
 This file's contents CAN BE DELETED, the comments, the example code etc. are not required and are
 just used to showcase common usage!

 NOTICE !!
 If you're developing a stand alone website(and not a component), please ignore these examples and
 refer to the documentation
 http://tlv-web.galacoral.com/docs/reusable-webpack-config/tutorial-developersWritingAstaticWebsite.html
 */

// we need react for the JSX and our example component
import React from 'react';
import MyDiv from './index';
import * as StyleExport from './index.scss';
// import our render function to test-render our component
// we use this function to render our components in the dev server
import exampleRender from 'reusable-webpack-config/scripts/util/render';


// otherwise render here
exampleRender(
  'This was easy...', // This will generate an H2 tag
  `This is some very long text
  regarding components in general, Lorem Ipsum
  `, // this text block will be used as a description above your component
  <MyDiv/>
);
